package com.oohwhoa.oohwhoa.entity.common;

import com.oohwhoa.oohwhoa.entity.user.UserBaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class RolesEntity {

  @Id
  private Long roleId;

  @Column(name = "ROLE_NAME", nullable = false, unique = true, length = 20)
  private String roleName;

  @OneToMany(mappedBy = "roleEntity")
  private List<UserBaseEntity> userEntityList = new ArrayList<>();

}

package com.oohwhoa.oohwhoa.entity.common;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import org.hibernate.annotations.Comment;
import java.time.LocalDateTime;

public class BaseEntity {

  @NotNull
  @Comment("MEMBER ID VAL")
  private String createUser;

  @NotNull
  private LocalDateTime createDate;

  @NotNull
  @Comment("MEMBER ID VAL")
  private String updateUser;

  @NotNull
  private LocalDateTime updateDate;

}

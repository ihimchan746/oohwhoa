package com.oohwhoa.oohwhoa.entity.skills;

import lombok.Getter;
import org.hibernate.annotations.Comment;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
public class SkillsEntity {

  @Id
  private Long skillNumber;

  @Comment("skill 이름")
  private String skillName;

  @Comment("skill Type - framework, tool or etc")
  private String skillType;



}

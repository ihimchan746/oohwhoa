package com.oohwhoa.oohwhoa.entity.user;


import com.oohwhoa.oohwhoa.entity.common.RolesEntity;
import lombok.Data;
import org.hibernate.annotations.Comment;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class UserBaseEntity {

  @Id
  @Column(name = "user_id", nullable = false, length = 10)
  @Comment("OW+년도월+5자리 번호 : EX) OW23040000")
  private String id;

  @Column(name = "user_pwd", nullable = false, length = 100)
  @Comment("비밀번호")
  private String password;

  @Column(name = "first_name", nullable = false, length = 50)
  @Comment("성")
  private String firstName;

  @Column(name = "middle_name", nullable = true, length = 50)
  @Comment("중간 이름")
  private String middleName;

  @Column(name = "last_name", nullable = false, length = 50)
  @Comment("이름")
  private String lastName;

  @Column(name = "date_of_birth", nullable = false)
  @Comment("생년월일")
  private LocalDate dateOfBirth;

  @Column(name = "phone_number", nullable = false, unique = true, length = 50)
  @Comment("핸드폰 번호")
  private String phoneNumber;

  @Column(name = "email", nullable = false, unique = true, length = 50)
  @Comment("이메일")
  private String email;

  @Column(nullable = false)
  @Comment("회원 가입 날짜")
  private LocalDateTime createdAt;

  @Column(nullable = false)
  @Comment("회원 가입을 진행한 진행자 (인사관리자)")
  private LocalDateTime createdBy;

  @Column(nullable = false)
  private LocalDateTime lastLoginAt;

  @ManyToOne
  @JoinColumn(name ="roleId")
  private RolesEntity roleEntity;

  public UserBaseEntity() {


  }

}

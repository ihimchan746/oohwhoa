package com.oohwhoa.oohwhoa.entity.member;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class MemberEntity {

  @Id
  @GeneratedValue
  private Long id;

  private String username;

}

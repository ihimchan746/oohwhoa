package com.oohwhoa.oohwhoa.entity.introduce;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class IntroduceEntity {
	@Id
	private Long id;

}

package com.oohwhoa.oohwhoa.controller.security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class SecurityController {

  @RequestMapping("/sign-up")
  public String getSignUp() {
    return "login/sign-up";
  }

  @RequestMapping("/sign-in")
  public String getSignIn() {
    return "login/sign-in";
  }

}

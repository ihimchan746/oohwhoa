package com.oohwhoa.oohwhoa.controller.blog;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequestMapping("/blog")
public class BlogController {

  @GetMapping("")
  public String getBlogPage() {
    log.info("getBlogPage");
    return "blog/index";
  }


}

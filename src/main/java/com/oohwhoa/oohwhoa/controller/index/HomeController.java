package com.oohwhoa.oohwhoa.controller.index;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class HomeController {

  @GetMapping(value = "")
  public String getIndexPage(HttpServletRequest request) {
    return "index";
  }


}

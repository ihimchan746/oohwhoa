package com.oohwhoa.oohwhoa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .authorizeRequests()
        .antMatchers("/", "/index", "/assets/**").permitAll()
        .anyRequest()
        .authenticated()
            .and()
        .formLogin()
        .loginPage("/login/sign-in").permitAll()
            .and()
        .logout()
        .permitAll();
  }


  @Bean
  @Override
  public UserDetailsService userDetailsService() {
    UserDetails user =
            User.withDefaultPasswordEncoder()
                    .username("user")
                    .password("password")
                    .roles("USER")
                    .build();

    return new InMemoryUserDetailsManager(user);
  }
    /*
    이 userDetailsService()메서드는 인스턴스를 생성하고
    이름, 패스워드, 권한을 부여하고 리턴하여
    각 유저들을 user store 메모리에서 설정합니다.
    */
}

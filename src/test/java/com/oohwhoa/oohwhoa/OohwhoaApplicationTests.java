package com.oohwhoa.oohwhoa;

import groovy.util.logging.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

@SpringBootTest
@Log4j2
class OohwhoaApplicationTests {

  /**
   # 2023-04-22 토 공부한 내용

    JPA 를 이해하는데 가장 중요한 용어 - 영속성 컨텍스트 persistence context
    Entity를 영구 저장 하는 환경
    Entity 매니저로 저장하거나 조회를 하면 영속성 컨텍스트에서 entity를 보관하고 관리함

    특징으로는 눈에 보이지 않고,
    entityManager 을 생성할 때 하나가 만들어진다.

    -- 엔티티 매니저를 통해 영속성 컨텍스트에 접근 혹은 관리를 할 수 있다.

    엔티티 생명주기 4가지
    비영속, 영속, 준영속, 삭제

    엔티티 생명주기 표 찾아 볼것.
   */
  @Test
  void contextLoads() {

    // 공장 만들기 - 비용이 아주 많이 든다.
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("oohwhoa");

    // 공장에서 엔티티 매니저 생성, 비용이 거의 안 든다.
    EntityManager em = emf.createEntityManager();

    // 트랜젝션 획득
    EntityTransaction tx = em.getTransaction();

    try {

      tx.begin(); // transaction start
      logic(em);  // business logic

      tx.commit(); // transaction start

    } catch (Exception e) {
      tx.rollback();
      throw new RuntimeException(e);
    } finally {
      em.close();
    }

    emf.close();
  }

  private void logic(EntityManager em) {
    System.out.println("start to business logic :: " + em.toString());
//    UserBaseEntity userBaseEntity = new UserBaseEntity();
//
//    // admin 계정 임시 발급
//    userBaseEntity.setId("OW230499999");
//    userBaseEntity.setFirstName("우와");
//    userBaseEntity.setLastName("관리자");

//    em.persist(userBaseEntity);
  }
}

## Oohwhoa's PROJECT
* SITE URL : http://www.oohwhoa.com
* JIRA URL : https://oohwhoa-doc.atlassian.net/jira/core/projects/OHW/board

## PROJECT record

* 2023-03-31
  * **1차 배포**
    * himdol's page 1차 완성

* 2023-08-25
  * **2차 배포**
    * [BO] domain 개발
    * [인프라] AWS
    * [인프라] Jenkins
    * [인프라] JIRA 연동

참고 : 2023년 4월 23일 부로 README.md 업데이트 종료 -> JIRA 로 변경
